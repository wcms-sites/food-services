core = 7.x
api = 2

; hours_of_operation
; Brand new. Hours of operation page. Could potentially be merged into uw_ct_food_outlet.
projects[hours_of_operation][type] = "module"
projects[hours_of_operation][download][type] = "git"
projects[hours_of_operation][download][url] = "https://git.uwaterloo.ca/wcms/hours_of_operation.git"
projects[hours_of_operation][download][tag] = "7.x-1.1"
projects[hours_of_operation][subdir] = ""

; Office hours
; patched version in advance of it being available in the profile.
; Base: 2d49f7c (7.x-1.9+3-dev)
; Patch dc2b0f3: Issue #3167972: Fix invalid HTML in theme_office_hours_field_formatter_default(). https://www.drupal.org/project/office_hours/issues/3167972#comment-13804546
projects[office_hours][type] = "module"
projects[office_hours][download][type] = "git"
projects[office_hours][download][url] = "https://git.uwaterloo.ca/drupal-org/office_hours.git"
projects[office_hours][download][tag] = "7.x-1.9+3-dev-uw_wcms1"
projects[office_hours][subdir] = ""

; Quicktabs
projects[quicktabs][type] = "module"
projects[quicktabs][download][type] = "git"
projects[quicktabs][download][url] = "https://git.uwaterloo.ca/drupal-org/quicktabs.git"
projects[quicktabs][download][tag] = "7.x-3.8"
projects[quicktabs][subdir] = ""

; uw_campus_locations
; Brand new. Locations page. Could potentially be merged into uw_ct_food_outlet.
projects[uw_campus_locations][type] = "module"
projects[uw_campus_locations][download][type] = "git"
projects[uw_campus_locations][download][url] = "https://git.uwaterloo.ca/wcms/uw_campus_locations.git"
projects[uw_campus_locations][download][tag] = "7.x-1.0"
projects[uw_campus_locations][subdir] = ""

; uw_ct_daily_menu
; Brand new. "Rotational" menu.
projects[uw_ct_daily_menu][type] = "module"
projects[uw_ct_daily_menu][download][type] = "git"
projects[uw_ct_daily_menu][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_daily_menu.git"
projects[uw_ct_daily_menu][download][tag] = "7.x-1.0"
projects[uw_ct_daily_menu][subdir] = ""

; uw_ct_food
; Brand new. Individual food items.
projects[uw_ct_food][type] = "module"
projects[uw_ct_food][download][type] = "git"
projects[uw_ct_food][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_food.git"
projects[uw_ct_food][download][tag] = "7.x-1.0"
projects[uw_ct_food][subdir] = ""

; uw_ct_food_outlet
; Branch of live module.
projects[uw_ct_food_outlet][type] = "module"
projects[uw_ct_food_outlet][download][type] = "git"
projects[uw_ct_food_outlet][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_food_outlet.git"
projects[uw_ct_food_outlet][download][tag] = "7.x-2.1"
projects[uw_ct_food_outlet][subdir] = ""

; uw_ct_franchise
; Brand new. Feature menus for food outlets.
projects[uw_ct_franchise][type] = "module"
projects[uw_ct_franchise][download][type] = "git"
projects[uw_ct_franchise][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_franchise.git"
projects[uw_ct_franchise][download][tag] = "7.x-1.2"
projects[uw_ct_franchise][subdir] = ""

; uw_food_services
; Branch of live module.
projects[uw_food_services][type] = "module"
projects[uw_food_services][download][type] = "git"
projects[uw_food_services][download][url] = "https://git.uwaterloo.ca/wcms/uw_food_services.git"
projects[uw_food_services][download][tag] = "7.x-3.0"
projects[uw_food_services][subdir] = ""
